package by.bsuir.simp.lab5.test;

import by.bsuir.simp.lab5.cell.Cell;

import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

abstract class AbstractTester implements Tester {

    void iterate(List<Cell> memory, Consumer<Integer> consumer) {
        for (int i = 0; i < memory.size(); i++) {
            consumer.accept(i);
        }
    }

    void iterateReverse(List<Cell> memory, Consumer<Integer> consumer) {
        for (int i = memory.size() - 1; i >= 0; i--) {
            consumer.accept(i);
        }
    }

    void write0(List<Cell> memory, int i) {
        memory.get(i).setValue(false);
    }

    void write1(List<Cell> memory, int i) {
        memory.get(i).setValue(true);
    }

    void read0(List<Cell> memory, int i, Set<Integer> malfunctions) {
        if (memory.get(i).getValue()) {
            malfunctions.add(i);
        }
    }

    void read1(List<Cell> memory, int i, Set<Integer> malfunctions) {
        if (!memory.get(i).getValue()) {
            malfunctions.add(i);
        }
    }
}

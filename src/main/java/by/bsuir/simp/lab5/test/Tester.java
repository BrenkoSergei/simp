package by.bsuir.simp.lab5.test;

import by.bsuir.simp.lab5.cell.Cell;

import java.util.List;
import java.util.Set;

public interface Tester {

    Set<Integer> test(List<Cell> memory);

    long getCountOfAccesses(long memorySize);
}

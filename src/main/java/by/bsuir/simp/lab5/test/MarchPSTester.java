package by.bsuir.simp.lab5.test;

import by.bsuir.simp.lab5.cell.Cell;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MarchPSTester extends AbstractTester {

    @Override
    public Set<Integer> test(List<Cell> memory) {
        Set<Integer> indexes = new HashSet<>();

        iterate(memory, i ->
                write0(memory, i)
        );

        iterate(memory, i -> {
            read0(memory, i, indexes);
            write1(memory, i);
            read1(memory, i, indexes);
            write0(memory, i);
            read0(memory, i, indexes);
            write1(memory, i);
        });

        iterate(memory, i -> {
            read1(memory, i, indexes);
            write0(memory, i);
            read0(memory, i, indexes);
            write1(memory, i);
            read1(memory, i, indexes);
        });

        iterate(memory, i -> {
            read1(memory, i, indexes);
            write0(memory, i);
            read0(memory, i, indexes);
            write1(memory, i);
            read1(memory, i, indexes);
            write0(memory, i);
        });

        iterate(memory, i -> {
            read0(memory, i, indexes);
            write1(memory, i);
            read1(memory, i, indexes);
            write0(memory, i);
            read0(memory, i, indexes);
        });

        return indexes;
    }

    @Override
    public long getCountOfAccesses(long memorySize) {
        return 23 * memorySize;
    }

    @Override
    public String toString() {
        return "March PS";
    }
}

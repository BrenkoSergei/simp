package by.bsuir.simp.lab5.cell;

public interface Cell {

    boolean getValue();

    void setValue(boolean value);
}

package by.bsuir.simp.lab5.malfunction;

import by.bsuir.simp.lab5.cell.Cell;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Pnpsfk9Modeler implements Modeler {

    private final int repeats;
    private final Random random = new Random(System.currentTimeMillis());

    public Pnpsfk9Modeler(int repeats) {
        if (repeats <= 0) {
            throw new IllegalArgumentException();
        }
        this.repeats = repeats;
    }

    @Override
    public Set<Integer> model(List<Cell> memory) {
        int variances = 256; // 2^8

        Set<Integer> baseCellIndexes = new HashSet<>();

        int[] indexes = random.ints(0, memory.size())
                .distinct()
                .limit(repeats * variances * 9)
                .toArray();

        for (int i = 0; i < repeats; i++) {
            for (int j = 0; j < variances; j++) {
                List<Integer> cellsToTest = IntStream.of(indexes)
                        .skip(i * variances * 9 + j * 9)
                        .limit(9)
                        .boxed()
                        .collect(Collectors.toList());
                int baseCellIndex = cellsToTest.get(4);
                baseCellIndexes.add(baseCellIndex);
                cellsToTest.remove(4);

                modelMalfunction(memory, baseCellIndex, cellsToTest, toBooleanArray(j));
            }
        }

        return baseCellIndexes;
    }

    @Override
    public String toString() {
        return "PNPSFK9 Modeler";
    }

    private boolean[] toBooleanArray(int integer) {
        boolean[] bits = new boolean[8];
        for (int i = bits.length - 1; i >= 0; i--) {
            bits[i] = (integer & (1 << i)) != 0;
        }
        return bits;
    }

    private void modelMalfunction(List<Cell> memory, Integer targetCell, List<Integer> neighbourIndexes, boolean... neighbourValues) {
        if (neighbourValues.length != 8) {
            throw new IllegalArgumentException();
        }
        Map<Cell, Boolean> neighbourToPatternValue = new HashMap<>();
        neighbourToPatternValue.put(memory.get(neighbourIndexes.get(0)), neighbourValues[0]);
        neighbourToPatternValue.put(memory.get(neighbourIndexes.get(1)), neighbourValues[1]);
        neighbourToPatternValue.put(memory.get(neighbourIndexes.get(2)), neighbourValues[2]);
        neighbourToPatternValue.put(memory.get(neighbourIndexes.get(3)), neighbourValues[3]);
        neighbourToPatternValue.put(memory.get(neighbourIndexes.get(4)), neighbourValues[4]);
        neighbourToPatternValue.put(memory.get(neighbourIndexes.get(5)), neighbourValues[5]);
        neighbourToPatternValue.put(memory.get(neighbourIndexes.get(6)), neighbourValues[6]);
        neighbourToPatternValue.put(memory.get(neighbourIndexes.get(7)), neighbourValues[7]);

        Pnpsfk9BaseCell baseCell = new Pnpsfk9BaseCell(memory.get(targetCell), neighbourToPatternValue);
        memory.set(targetCell, baseCell);
    }

    private static class Pnpsfk9BaseCell implements Cell {

        private final Cell targetCell;
        private final Map<Cell, Boolean> patterns;

        private Pnpsfk9BaseCell(Cell targetCell, Map<Cell, Boolean> patterns) {
            this.targetCell = targetCell;
            this.patterns = patterns;
        }

        @Override
        public boolean getValue() {
            return this.targetCell.getValue();
        }

        @Override
        public void setValue(boolean value) {
            int counter = 0;
            for (Cell cell : patterns.keySet()) {
                Boolean patternValue = patterns.get(cell);
                if (cell.getValue() != patternValue) {
                    ++counter;
                }
            }
            if (counter != patterns.size()) {
                this.targetCell.setValue(value);
            }
        }
    }
}

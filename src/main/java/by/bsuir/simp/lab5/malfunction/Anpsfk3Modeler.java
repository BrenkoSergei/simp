package by.bsuir.simp.lab5.malfunction;

import by.bsuir.simp.lab5.cell.Cell;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Anpsfk3Modeler implements Modeler {

    private final int repeats;
    private final Random random = new Random(System.currentTimeMillis());

    public Anpsfk3Modeler(int repeats) {
        if (repeats <= 0) {
            throw new IllegalArgumentException();
        }
        this.repeats = repeats;
    }

    @Override
    public Set<Integer> model(List<Cell> memory) {
        int variances = 16; //2^(3-1)^2

        Set<Integer> baseCellIndexes = new HashSet<>();

        int[] indexes = random.ints(0, memory.size())
                .distinct()
                .limit(repeats * variances * 3)
                .toArray();

        for (int i = 0; i < repeats; i++) {
            for (int j = 0; j < variances; j++) {
                List<Integer> cellsToTest = IntStream.of(indexes)
                        .skip(i * variances * 3 + j * 3)
                        .limit(9)
                        .boxed()
                        .collect(Collectors.toList());
                int baseCellIndex = cellsToTest.get(1);
                baseCellIndexes.add(baseCellIndex);
                cellsToTest.remove(1);

                modelMalfunction(memory, baseCellIndex, cellsToTest, toBooleanArray(j));
            }
        }

        return baseCellIndexes;
    }

    @Override
    public String toString() {
        return "ANPSFK3 Modeler";
    }

    private boolean[] toBooleanArray(int integer) {
        boolean[] bits = new boolean[4];
        for (int i = bits.length - 1; i >= 0; i--) {
            bits[i] = (integer & (1 << i)) != 0;
        }
        return bits;
    }

    private void modelMalfunction(List<Cell> memory, Integer targetCell, List<Integer> neighbourIndexes, boolean... targetValues) {
        if (targetValues.length != 4) {
            throw new IllegalArgumentException();
        }

        Cell target = memory.get(targetCell);
        Cell neighbour1 = memory.get(neighbourIndexes.get(0));
        Cell neighbour2 = memory.get(neighbourIndexes.get(1));

        Map<Map<Cell, Boolean>, Boolean> neighboursMappingToValue = new HashMap<>();
        neighboursMappingToValue.put(generateNeighboursMapping(neighbour1, false, neighbour2, false), targetValues[0]);
        neighboursMappingToValue.put(generateNeighboursMapping(neighbour1, false, neighbour2, true), targetValues[1]);
        neighboursMappingToValue.put(generateNeighboursMapping(neighbour1, true, neighbour2, false), targetValues[2]);
        neighboursMappingToValue.put(generateNeighboursMapping(neighbour1, true, neighbour2, true), targetValues[3]);

        Anpsfk3BaseCell baseCellDecorator = new Anpsfk3BaseCell(target, neighboursMappingToValue);
        memory.set(targetCell, baseCellDecorator);
        memory.set(neighbourIndexes.get(0), new Anpsfk3NeighbourCell(neighbour1, baseCellDecorator));
        memory.set(neighbourIndexes.get(1), new Anpsfk3NeighbourCell(neighbour2, baseCellDecorator));
    }

    private Map<Cell, Boolean> generateNeighboursMapping(Cell neighbour1, Boolean neighbour1Value, Cell neighbour2, Boolean neighbour2Value) {
        Map<Cell, Boolean> neighboursMapping = new HashMap<>();
        neighboursMapping.put(neighbour1, neighbour1Value);
        neighboursMapping.put(neighbour2, neighbour2Value);
        return neighboursMapping;
    }

    private static class Anpsfk3BaseCell implements Cell {

        private final Cell targetCell;
        private final Map<Map<Cell, Boolean>, Boolean> patterns;

        private Anpsfk3BaseCell(Cell targetCell, Map<Map<Cell, Boolean>, Boolean> patterns) {
            this.targetCell = targetCell;
            this.patterns = patterns;
        }

        private void resetValue() {
            main:
            for (Map.Entry<Map<Cell, Boolean>, Boolean> patternToValue : patterns.entrySet()) {
                Map<Cell, Boolean> patternMap = patternToValue.getKey();
                for (Cell cell : patternMap.keySet()) {
                    if (cell.getValue() != patternMap.get(cell)) {
                        continue main;
                    }
                }
                this.targetCell.setValue(patternToValue.getValue());
                break;
            }
        }

        @Override
        public boolean getValue() {
            return this.targetCell.getValue();
        }

        @Override
        public void setValue(boolean value) {
            this.targetCell.setValue(value);
        }
    }

    private static class Anpsfk3NeighbourCell implements Cell {

        private final Cell targetCell;
        private final Anpsfk3BaseCell baseCell;

        private Anpsfk3NeighbourCell(Cell targetCell, Anpsfk3BaseCell patterns) {
            this.targetCell = targetCell;
            this.baseCell = patterns;
        }

        @Override
        public boolean getValue() {
            return this.targetCell.getValue();
        }

        @Override
        public void setValue(boolean value) {
            this.targetCell.setValue(value);
            this.baseCell.resetValue();
        }
    }
}

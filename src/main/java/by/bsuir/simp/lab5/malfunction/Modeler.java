package by.bsuir.simp.lab5.malfunction;

import by.bsuir.simp.lab5.cell.Cell;

import java.util.List;
import java.util.Set;

public interface Modeler {

    Set<Integer> model(List<Cell> memory);
}

package by.bsuir.simp.lab5;

import by.bsuir.simp.lab5.analizator.TimeAnalizator;
import by.bsuir.simp.lab5.cell.Cell;
import by.bsuir.simp.lab5.cell.SimpleCell;
import by.bsuir.simp.lab5.malfunction.Anpsfk3Modeler;
import by.bsuir.simp.lab5.malfunction.Modeler;
import by.bsuir.simp.lab5.malfunction.Pnpsfk9Modeler;
import by.bsuir.simp.lab5.test.MarchPSTester;
import by.bsuir.simp.lab5.test.MatsPlusPlusTester;
import by.bsuir.simp.lab5.test.Tester;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Lab5Runner {

    public static void main(String[] args) {
        TimeAnalizator timeAnalizator = new TimeAnalizator(100_000_000);

        for (Modeler modeler : getModelers()) {
            List<Cell> memory = generateMemory();
            Set<Integer> reservedIndexes = modeler.model(memory);

            for (Tester tester : getTesters()) {
                Set<Integer> testResults = tester.test(memory);
                System.out.println(modeler + " - " + tester);
                System.out.println(concatSorted(reservedIndexes));
                System.out.println(concatSorted(testResults));
                System.out.println("Coverage: " + (testResults.size() * 1.0 / reservedIndexes.size()) * 100 + "%");
                System.out.println("Time: " + timeAnalizator.analyzeDuration(tester, memory) + "\n\n\n");
            }
        }
    }

    private static List<Cell> generateMemory() {
        return Stream.generate(SimpleCell::new)
                .limit(1024 * 1024)
                .collect(Collectors.toList());
    }

    private static List<Modeler> getModelers() {
        return Arrays.asList(
                new Pnpsfk9Modeler(10),
                new Anpsfk3Modeler(10)
        );
    }

    private static List<Tester> getTesters() {
        return Arrays.asList(
                new MatsPlusPlusTester(),
                new MarchPSTester()
        );
    }

    private static <T> String concatSorted(Set<? extends Comparable<T>> values) {
        return values.stream()
                .sorted()
                .map(Object::toString)
                .collect(Collectors.joining(", "));
    }
}

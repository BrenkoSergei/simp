package by.bsuir.simp.lab3;

import by.bsuir.simp.lab2.Lab2Runner;
import by.bsuir.simp.lab2.cube.Operand;
import by.bsuir.simp.lab2.cube.Symbol;
import by.bsuir.simp.lab2.graph.InputNode;
import by.bsuir.simp.lab2.graph.Node;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Lab3Runner {

    public static void main(String[] args) {
        InputNode inputNode0 = new InputNode(0);
        InputNode inputNode1 = new InputNode(1);
        InputNode inputNode2 = new InputNode(2);
        InputNode inputNode3 = new InputNode(3);
        InputNode inputNode4 = new InputNode(4);
        InputNode inputNode5 = new InputNode(5);
        InputNode inputNode6 = new InputNode(6);
        List<InputNode> inputNodes = Arrays.asList(inputNode0, inputNode1, inputNode2, inputNode3, inputNode4, inputNode5, inputNode6);

        Node f1 = new Node("F1", Operand.NOT_AND_2, inputNode0, inputNode1);
        Node f2 = new Node("F2", Operand.NOT, inputNode2);
        Node f3 = new Node("F3", Operand.NOT_OR_2, inputNode4, inputNode5);
        Node f4 = new Node("F4", Operand.AND_3, inputNode3, f3, inputNode6);
        Node f5 = new Node("F5", Operand.OR_2, f2, f4);
        Node f6 = new Node("F6", Operand.AND_2, f1, f5);
        List<Node> nodes = Arrays.asList(f1, f2, f3, f4, f5, f6);

        Set<Map<String, Symbol>> namedTestInputs = Lab2Runner.findTestInputs(inputNodes, nodes, nodes);
        Set<Integer> testInputs = convertNamedTestInputs(namedTestInputs);

        List<Statistic> statistics = new ArrayList<>();
        for (int i = 1; i < 128; i++) {
            Set<Integer> testSuits = new HashSet<>(testInputs);
            Lfsr lfsr = new Lfsr(Arrays.asList(3, 4, 5, 7), i, 8);

            int generationAttempts = 5000;
            while (!testSuits.isEmpty() && generationAttempts != 0) {
                int lfsrOutput = lfsr.generate(inputNodes.size());
                testSuits.remove(lfsrOutput);
                generationAttempts--;
            }

            statistics.add(new Statistic(lfsr.getInitialValue(), lfsr.getCyclesCounter(), generationAttempts == 0, testSuits));
        }

        printNotFailedRuns(statistics);
        printFailedRuns(statistics);
    }

    private static void printNotFailedRuns(List<Statistic> statistics) {
        if (statistics.stream().anyMatch(Statistic::isNotFailed)) {
            System.out.println("==== Not failed runs ====");
            statistics.stream()
                    .filter(Statistic::isNotFailed)
                    .sorted(Comparator.comparing(Statistic::getCycles))
                    .forEach(statistic -> System.out.println(String.format(
                            "Initial value = %s (%3d) -> Number of cycles = %5d",
                            String.format("%7s", Integer.toBinaryString(statistic.getRegisterInitialValue())).replace(" ", "0"),
                            statistic.getRegisterInitialValue(),
                            statistic.getCycles()))
                    );
        }
    }

    private static void printFailedRuns(List<Statistic> statistics) {
        if (statistics.stream().anyMatch(Statistic::isFailed)) {
            System.out.println("==== Failed runs ====");
            statistics.stream()
                    .filter(Statistic::isFailed)
                    .sorted(Comparator.comparing(Statistic::getCycles))
                    .forEach(statistic -> System.out.println(String.format(
                            "Failed initial values = %s (%3d) -> %s",
                            String.format("%7s", Integer.toBinaryString(statistic.getRegisterInitialValue())).replace(" ", "0"),
                            statistic.getRegisterInitialValue(),
                            statistic.getLeftElements()))
                    );
        }
    }

    private static Set<Integer> convertNamedTestInputs(Set<Map<String, Symbol>> namedTestInputs) {
        Set<Integer> testInputs = new HashSet<>();
        for (Map<String, Symbol> namedTestInput : namedTestInputs) {
            Set<String> keySet = namedTestInput.keySet();

            int temp = 0;
            for (String key : keySet) {
                temp <<= 1;
                Symbol symbol = namedTestInput.get(key);
                temp += symbol == Symbol.ONE ? 1 : 0;
            }
            testInputs.add(temp);
        }
        return testInputs;
    }
}

package by.bsuir.simp.lab3;

import java.util.List;

public class Lfsr {

    private final List<Integer> polinomIndexes;
    private final int initialValue;
    private final int registerSize;

    private int cyclesCounter;
    private int register;

    public Lfsr(List<Integer> polinomIndexes, int initialValue, int registerSize) {
        this.polinomIndexes = polinomIndexes;
        this.initialValue = initialValue;
        this.registerSize = registerSize;

        this.cyclesCounter = 0;
        this.register = initialValue;
    }

    public int generate(int numberOfBits) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < numberOfBits; i++) {
            stringBuilder.append(nextBit());
        }
        return Integer.parseInt(stringBuilder.toString(), 2);
    }

    public int nextBit() {
        ++cyclesCounter;

        int outputBit = register & 1;
        int generatedBit = generateNewBit();

        register >>= 1;
        if (generatedBit == 1) {
            register |= (1 << registerSize - 1);
        } else {
            register &= ~(1 << registerSize - 1);
        }

        return outputBit;
    }

    private int generateNewBit() {
        int generatedBit = (register >> (registerSize - 1 - polinomIndexes.get(0))) & 1;
        for (int i = 1; i < polinomIndexes.size(); i++) {
            generatedBit ^= (register >> (registerSize - 1 - polinomIndexes.get(i))) & 1;
        }
        return generatedBit;
    }

    public int getInitialValue() {
        return initialValue;
    }

    public int getCyclesCounter() {
        return cyclesCounter;
    }
}

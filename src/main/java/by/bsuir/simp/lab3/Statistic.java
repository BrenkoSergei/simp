package by.bsuir.simp.lab3;

import java.util.Set;

public class Statistic {

    private final int registerInitialValue;
    private final int cycles;
    private final boolean isFailed;
    private final Set<Integer> leftElements;

    public Statistic(int registerInitialValue, int cycles, boolean isFailed, Set<Integer> leftElements) {
        this.registerInitialValue = registerInitialValue;
        this.cycles = cycles;
        this.isFailed = isFailed;
        this.leftElements = leftElements;
    }

    public int getRegisterInitialValue() {
        return registerInitialValue;
    }

    public long getCycles() {
        return cycles;
    }

    public boolean isFailed() {
        return isFailed;
    }

    public boolean isNotFailed() {
        return !isFailed;
    }

    public Set<Integer> getLeftElements() {
        return leftElements;
    }
}

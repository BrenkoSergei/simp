package by.bsuir.simp.lab4.cell;

public interface Cell {

    boolean getValue();

    void setValue(boolean value);
    
//    boolean isDirty();
}

package by.bsuir.simp.lab4.cell;

public class SimpleCell implements Cell {

    private boolean value;

    @Override
    public boolean getValue() {
        return value;
    }

    @Override
    public void setValue(boolean value) {
        this.value = value;
    }
}

package by.bsuir.simp.lab4.analizator;

import by.bsuir.simp.lab4.cell.Cell;
import by.bsuir.simp.lab4.test.Tester;

import java.time.Duration;
import java.util.List;

public class TimeAnalizator {

    private final long memoryFrequencyHz;

    public TimeAnalizator(long memoryFrequencyHz) {
        this.memoryFrequencyHz = memoryFrequencyHz;
    }

    public Duration analyzeDuration(Tester tester, List<Cell> memory) {
        long countOfAccesses = tester.getCountOfAccesses(memory.size());
        long seconds = countOfAccesses * 1000 / memoryFrequencyHz;
        return Duration.ofMillis(seconds);
    }
}

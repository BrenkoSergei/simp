package by.bsuir.simp.lab4.malfunction;

import java.util.Random;
import java.util.Set;

public abstract class AbstractModeler implements Modeler {

    private final Random random = new Random(System.currentTimeMillis());

    int[] generateRandomIndexes(int maxValue, int limit, Set<Integer> reservedValues) {
        return random.ints(0, maxValue)
                .distinct()
                .filter(value -> !reservedValues.contains(value))
                .limit(limit)
                .toArray();
    }
}

package by.bsuir.simp.lab4.malfunction;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import by.bsuir.simp.lab4.cell.Cell;

public class CFinModeler extends AbstractModeler {

    private final int type1;
    private final int type2;
    private final int type3;
    private final int type4;

    public CFinModeler(int targetCellsCount) {
        if (targetCellsCount < 4) {
            throw new IllegalArgumentException();
        }
        int half1 = targetCellsCount / 2;
        int half2 = targetCellsCount - half1;

        this.type1 = half1 / 2;
        this.type2 = half1 - this.type1;
        this.type3 = half2 / 2;
        this.type4 = half2 - this.type3;
    }

    @Override
    public Set<Integer> model(List<Cell> memory, Set<Integer> reservedIndexes) {
        int[] targetCells = generateRandomIndexes(memory.size(), (type1 + type2 + type3 + type4) * 2, reservedIndexes);

        int[] type1Cells = Arrays.stream(targetCells).limit(type1 * 2).toArray();
        modelAggressorBefore(memory, type1Cells, true);

        int[] type2Cells = Arrays.stream(targetCells).skip(type1 * 2).limit(type2 * 2).toArray();
        modelAggressorBefore(memory, type2Cells, false);

        int[] type3Cells = Arrays.stream(targetCells).skip((type1 + type2) * 2).limit(type3 * 2).toArray();
        modelAggressorAfter(memory, type3Cells, true);

        int[] type4Cells = Arrays.stream(targetCells).skip((type1 + type2 + type3) * 2).limit(type4 * 2).toArray();
        modelAggressorAfter(memory, type4Cells, false);

        return Arrays.stream(targetCells).boxed().collect(Collectors.toSet());
    }

    private void modelAggressorBefore(List<Cell> memory, int[] targetCells, boolean value) {
        for (int i = 0; i < targetCells.length; i += 2) {
            int aggressorCell = targetCells[i] < targetCells[i + 1] ? targetCells[i] : targetCells[i + 1];
            int victimCell = targetCells[i] < targetCells[i + 1] ? targetCells[i + 1] : targetCells[i];

            CFinAggressorCell cell = new CFinAggressorCell(memory.get(aggressorCell), memory.get(victimCell), value);
            memory.set(aggressorCell, cell);
        }
    }

    private void modelAggressorAfter(List<Cell> memory, int[] targetCells, boolean value) {
        for (int i = 0; i < targetCells.length; i += 2) {
            int aggressorCell = targetCells[i] < targetCells[i + 1] ? targetCells[i + 1] : targetCells[i];
            int victimCell = targetCells[i] < targetCells[i + 1] ? targetCells[i] : targetCells[i + 1];

            CFinAggressorCell cell = new CFinAggressorCell(memory.get(aggressorCell), memory.get(victimCell), value);
            memory.set(aggressorCell, cell);
        }
    }

    @Override
    public String toString() {
        return "CFin Modeler";
    }

    private static class CFinAggressorCell implements Cell {

        private final Cell target;
        private final Cell victim;
        private final boolean value;

        CFinAggressorCell(Cell target, Cell victim, boolean value) {
            this.target = target;
            this.victim = victim;
            this.value = value;
        }

        @Override
        public boolean getValue() {
            return target.getValue();
        }

        @Override
        public void setValue(boolean value) {
            if (value == this.value && this.target.getValue() != value) {
//                this.target.setValue(value);
                this.victim.setValue(!this.victim.getValue());
            }
            this.target.setValue(value);
        }
    }
}

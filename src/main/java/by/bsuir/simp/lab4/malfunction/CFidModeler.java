package by.bsuir.simp.lab4.malfunction;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import by.bsuir.simp.lab4.cell.Cell;

public class CFidModeler extends AbstractModeler {

    private final int type1;
    private final int type2;
    private final int type3;
    private final int type4;
    private final int type5;
    private final int type6;
    private final int type7;
    private final int type8;

    public CFidModeler(int targetCellsCount) {
        if (targetCellsCount < 8) {
            throw new IllegalArgumentException();
        }
        int half1 = targetCellsCount / 2;
        int half2 = targetCellsCount - half1;

        int quarter1 = half1 / 2;
        int quarter2 = half1 - quarter1;
        int quarter3 = half2 / 2;
        int quarter4 = half2 - quarter3;

        this.type1 = quarter1 / 2;
        this.type2 = quarter1 - this.type1;
        this.type3 = quarter2 / 2;
        this.type4 = quarter2 - this.type3;
        this.type5 = quarter3 / 2;
        this.type6 = quarter3 - this.type5;
        this.type7 = quarter4 / 2;
        this.type8 = quarter4 - this.type7;
    }

    @Override
    public Set<Integer> model(List<Cell> memory, Set<Integer> reservedIndexes) {
        int totalCellsCount = (type1 + type2 + type3 + type4 + type5 + type6 + type7 + type8) * 2;
        int[] targetCells = generateRandomIndexes(memory.size(), totalCellsCount, reservedIndexes);

        int[] type1Cells = Arrays.stream(targetCells).limit(type1 * 2).toArray();
        modelAggressorBefore(memory, type1Cells, true, true);

        int[] type2Cells = Arrays.stream(targetCells).skip(type1 * 2).limit(type2 * 2).toArray();
        modelAggressorBefore(memory, type2Cells, true, false);

        int[] type3Cells = Arrays.stream(targetCells).skip((type1 + type2) * 2).limit(type3 * 2).toArray();
        modelAggressorBefore(memory, type3Cells, false, true);

        int[] type4Cells = Arrays.stream(targetCells).skip((type1 + type2 + type3) * 2).limit(type4 * 2).toArray();
        modelAggressorBefore(memory, type4Cells, false, false);

        int[] type5Cells = Arrays.stream(targetCells).skip((type1 + type2 + type3 + type4) * 2).limit(type5 * 2).toArray();
        modelAggressorAfter(memory, type5Cells, true, true);

        int[] type6Cells = Arrays.stream(targetCells).skip((type1 + type2 + type3 + type4 + type5) * 2).limit(type6 * 2).toArray();
        modelAggressorAfter(memory, type6Cells, true, false);

        int[] type7Cells = Arrays.stream(targetCells).skip((type1 + type2 + type3 + type4 + type5 + type6) * 2).limit(type6 * 2).toArray();
        modelAggressorAfter(memory, type7Cells, false, true);

        int[] type8Cells = Arrays.stream(targetCells).skip((type1 + type2 + type3 + type4 + type5 + type6 + type7) * 2).limit(type8 * 2).toArray();
        modelAggressorAfter(memory, type8Cells, false, false);

        return Arrays.stream(targetCells).boxed().collect(Collectors.toSet());
    }

    private void modelAggressorBefore(List<Cell> memory, int[] targetCells, boolean value, boolean victimValue) {
        for (int i = 0; i < targetCells.length; i += 2) {
            int aggressorCell = targetCells[i] < targetCells[i + 1] ? targetCells[i] : targetCells[i + 1];
            int victimCell = targetCells[i] < targetCells[i + 1] ? targetCells[i + 1] : targetCells[i];

            CFidAggressorCell cell = new CFidAggressorCell(memory.get(aggressorCell), memory.get(victimCell), value, victimValue);
            memory.set(aggressorCell, cell);
        }
    }

    private void modelAggressorAfter(List<Cell> memory, int[] targetCells, boolean value, boolean victimValue) {
        for (int i = 0; i < targetCells.length; i += 2) {
            int aggressorCell = targetCells[i] < targetCells[i + 1] ? targetCells[i + 1] : targetCells[i];
            int victimCell = targetCells[i] < targetCells[i + 1] ? targetCells[i] : targetCells[i + 1];

            CFidAggressorCell cell = new CFidAggressorCell(memory.get(aggressorCell), memory.get(victimCell), value, victimValue);
            memory.set(aggressorCell, cell);
        }
    }

    @Override
    public String toString() {
        return "CFid Modeler";
    }

    private static class CFidAggressorCell implements Cell {

        private final Cell target;
        private final Cell victim;
        private final boolean value;
        private final boolean victimValue;

        CFidAggressorCell(Cell target, Cell victim, boolean value, boolean victimValue) {
            this.target = target;
            this.victim = victim;
            this.value = value;
            this.victimValue = victimValue;
        }

        @Override
        public boolean getValue() {
            return target.getValue();
        }

        @Override
        public void setValue(boolean value) {
            if (value == this.value && this.target.getValue() != value) {
//                this.target.setValue(value);
                this.victim.setValue(this.victimValue);
            }
            this.target.setValue(value);
        }
    }
}

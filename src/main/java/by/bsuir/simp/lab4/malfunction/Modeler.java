package by.bsuir.simp.lab4.malfunction;

import java.util.List;
import java.util.Set;

import by.bsuir.simp.lab4.cell.Cell;

public interface Modeler {

    Set<Integer> model(List<Cell> memory, Set<Integer> reservedIndexes);
}

package by.bsuir.simp.lab4.malfunction;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import by.bsuir.simp.lab4.cell.Cell;

public class SAFModeler extends AbstractModeler {

    private final int targetCells1;
    private final int targetCells2;

    public SAFModeler(int targetCellsCount) {
        if (targetCellsCount < 2) {
            throw new IllegalArgumentException();
        }
        this.targetCells1 = targetCellsCount / 2;
        this.targetCells2 = targetCellsCount - this.targetCells1;
    }

    @Override
    public Set<Integer> model(List<Cell> memory, Set<Integer> reservedIndexes) {
        int[] targetCells = generateRandomIndexes(memory.size(), targetCells1 + targetCells2, reservedIndexes);

        int[] cells1 = Arrays.stream(targetCells).limit(targetCells1).toArray();
        for (int targetCell : cells1) {
            memory.set(targetCell, new SAFCell(true));
        }
        int[] cells2 = Arrays.stream(targetCells).skip(targetCells1).limit(targetCells2).toArray();
        for (int targetCell : cells2) {
            memory.set(targetCell, new SAFCell(false));
        }

        return Arrays.stream(targetCells).boxed().collect(Collectors.toSet());
    }

    @Override
    public String toString() {
        return "SAF Modeler";
    }

    private static class SAFCell implements Cell {

        private final boolean constantValue;

        SAFCell(boolean constantValue) {
            this.constantValue = constantValue;
        }

        @Override
        public boolean getValue() {
            return constantValue;
        }

        @Override
        public void setValue(boolean value) {
        }
    }
}

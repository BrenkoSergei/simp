package by.bsuir.simp.lab4.malfunction;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import by.bsuir.simp.lab4.cell.Cell;

public class AFSameCellModeler extends AbstractModeler {

    private static final int RANGE_OF_ACCESSORS = 3;

    private final int targetCellsCount;

    public AFSameCellModeler(int targetCellsCount) {
        this.targetCellsCount = targetCellsCount;
    }

    @Override
    public Set<Integer> model(List<Cell> memory, Set<Integer> reservedIndexes) {
        int[] targetCells = generateRandomIndexes(memory.size(), targetCellsCount * RANGE_OF_ACCESSORS, reservedIndexes);

        for (int i = 0; i < targetCells.length; i += 3) {
            Cell cell = memory.get(targetCells[i]);
            memory.set(targetCells[i], new AFCell(cell));
            memory.set(targetCells[i + 1], new AFCell(cell));
            memory.set(targetCells[i + 2], new AFCell(cell));
        }

        return Arrays.stream(targetCells).boxed().collect(Collectors.toSet());
    }

    @Override
    public String toString() {
        return "AF Modeler";
    }

    private static class AFCell implements Cell {

        private final Cell simpleCell;

        AFCell(Cell simpleCell) {
            this.simpleCell = simpleCell;
        }

        @Override
        public boolean getValue() {
            return simpleCell.getValue();
        }

        @Override
        public void setValue(boolean value) {
            simpleCell.setValue(value);
        }
    }
}

package by.bsuir.simp.lab4.test;

import by.bsuir.simp.lab4.cell.Cell;

import java.util.List;
import java.util.Set;

public interface Tester {

    Set<Integer> test(List<Cell> memory);

    long getCountOfAccesses(long memorySize);
}

package by.bsuir.simp.lab4.test;

import by.bsuir.simp.lab4.cell.Cell;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MatsPlusPlusTester implements Tester {

    @Override
    public Set<Integer> test(List<Cell> memory) {
        Set<Integer> indexes = new HashSet<>();

        for (Cell cell : memory) {
            cell.setValue(false);
        }

        for (int i = 0; i < memory.size(); i++) {
            if (memory.get(i).getValue()) {
                indexes.add(i);
            }
            memory.get(i).setValue(true);
        }

        for (int i = memory.size() - 1; i >= 0; i--) {
            if (!memory.get(i).getValue()) {
                indexes.add(i);
            }
            memory.get(i).setValue(false);
            if (memory.get(i).getValue()) {
                indexes.add(i);
            }
        }

        return indexes;
    }

    @Override
    public long getCountOfAccesses(long memorySize) {
        return 6 * memorySize;
    }

    @Override
    public String toString() {
        return "MATS++";
    }
}

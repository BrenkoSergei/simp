package by.bsuir.simp.lab4.test;

import by.bsuir.simp.lab4.cell.Cell;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Walking01Tester implements Tester {

    private static final boolean TEST_VALUE = false;

    @Override
    public Set<Integer> test(List<Cell> memory) {
        Set<Integer> indexes = new HashSet<>();

        for (int i = 0; i < memory.size(); i++) {
            memory.get(i).setValue(TEST_VALUE);
            for (int j = 0; j < memory.size(); j++) {
                if (j != i) {
                    memory.get(j).setValue(!TEST_VALUE);
                }
            }
            indexes.addAll(checkMemoryState(memory, i));
        }

        return indexes;
    }

    @Override
    public long getCountOfAccesses(long memorySize) {
        return memorySize * memorySize;
    }

    private Set<Integer> checkMemoryState(List<Cell> memory, int baseCell) {
        Set<Integer> malfunctionIndexes = new HashSet<>();

        for (int i = 0; i < memory.size(); i++) {
            if (i != baseCell && memory.get(i).getValue() == TEST_VALUE) {
                malfunctionIndexes.add(i);
            }
        }
        if (memory.get(baseCell).getValue() != TEST_VALUE) {
            malfunctionIndexes.add(baseCell);
        }

        return malfunctionIndexes;
    }

    @Override
    public String toString() {
        return "Walking 0/1";
    }
}

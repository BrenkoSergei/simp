package by.bsuir.simp.lab4;

import by.bsuir.simp.lab4.analizator.TimeAnalizator;
import by.bsuir.simp.lab4.cell.Cell;
import by.bsuir.simp.lab4.cell.SimpleCell;
import by.bsuir.simp.lab4.malfunction.AFSameCellModeler;
import by.bsuir.simp.lab4.malfunction.CFidModeler;
import by.bsuir.simp.lab4.malfunction.CFinModeler;
import by.bsuir.simp.lab4.malfunction.Modeler;
import by.bsuir.simp.lab4.malfunction.SAFModeler;
import by.bsuir.simp.lab4.test.MatsPlusPlusTester;
import by.bsuir.simp.lab4.test.Tester;
import by.bsuir.simp.lab4.test.Walking01Tester;

import java.time.Duration;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Lab4Runner {

    public static void main(String[] args) {
        List<Tester> testers = Stream.of(
                new Walking01Tester(),
                new MatsPlusPlusTester()
        ).collect(Collectors.toList());

        for (Tester tester : testers) {
            System.out.println(String.format("\n\n\n\n==========================      Test: %s      ==========================", tester.toString()));
            List<Cell> memory = generateMemory();

            Map<Modeler, Set<Integer>> modelerToReservedIndexes = modelMalfunctions(memory);
            modelerToReservedIndexes.forEach((modeler, modelerIndexes) -> {
                System.out.println(String.format("%s Reserved Indexes", modeler.toString()));
                System.out.println(concatSorted(modelerIndexes));
            });
            Set<Integer> allReservedIndexes = modelerToReservedIndexes.values().stream()
                    .flatMap(Collection::stream)
                    .collect(Collectors.toSet());

            Set<Integer> testResult = tester.test(memory);

            System.out.println("\n\nAll Reserved Indexes");
            System.out.println(concatSorted(allReservedIndexes));
            System.out.println("Test Results");
            System.out.println(concatSorted(testResult));

            System.out.println("\nModelers coverage");
            modelerToReservedIndexes.forEach((modeler, modelerIndexes) -> {
                Set<Integer> indexes = new HashSet<>(modelerIndexes);
                indexes.removeIf(testResult::contains);

                System.out.println(modeler + " coverage: " + (1 - indexes.size() * 1.0 / modelerIndexes.size()) * 100 + "%");
            });
        }

        System.out.println("\n\n\nTime Analizator metrics");
        TimeAnalizator timeAnalizator = new TimeAnalizator(100_000_000L);
        testers.forEach(tester -> {
            Duration duration = timeAnalizator.analyzeDuration(tester, generateMemory());
            System.out.println(tester + " time: " + duration);
        });
    }

    private static List<Cell> generateMemory() {
        return Stream.generate(SimpleCell::new)
                .limit(1024 * 64)
                .collect(Collectors.toList());
    }

    private static Map<Modeler, Set<Integer>> modelMalfunctions(List<Cell> memory) {
        List<Modeler> modelers = Stream.of(
                new AFSameCellModeler(100),
                new SAFModeler(100),
                new CFinModeler(100),
                new CFidModeler(104)
        ).collect(Collectors.toList());

        Map<Modeler, Set<Integer>> modelerToReservedIndexes = new HashMap<>();
        Set<Integer> reservedIndexes = new HashSet<>();

        for (Modeler modeler : modelers) {
            Set<Integer> newReservedIndexes = modeler.model(memory, reservedIndexes);
            modelerToReservedIndexes.put(modeler, newReservedIndexes);
            reservedIndexes.addAll(newReservedIndexes);
        }

        return modelerToReservedIndexes;
    }

    private static <T> String concatSorted(Set<? extends Comparable<T>> values) {
        return values.stream()
                .sorted()
                .map(Object::toString)
                .collect(Collectors.joining(", "));
    }
}

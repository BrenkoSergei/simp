package by.bsuir.simp.lab2.util;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

public final class CollectionUtils {

    public static <T> List<T> computeCartesianProduct(List<T> list1, List<T> list2, BiFunction<T, T, T> merger) {
        if (list1.isEmpty()) {
            return new ArrayList<>(list2);
        } else {
            return list1.stream()
                    .flatMap(element1 -> list2.stream()
                            .map(element2 -> merger.apply(element1, element2)))
                    .collect(Collectors.toList());
        }
    }
}

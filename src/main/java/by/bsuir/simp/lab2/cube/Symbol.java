package by.bsuir.simp.lab2.cube;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import by.bsuir.simp.lab2.util.Pair;

public enum Symbol {

    ONE("1"),
    ZERO("0"),
    X("X"),
    D("D"),
    _D("_D");

    private static final Map<Pair<Symbol, Symbol>, Symbol> SINGULAR_INTERSECTIONS = new HashMap<>();

    static {
        SINGULAR_INTERSECTIONS.put(Pair.of(ZERO, ZERO), ZERO);
        SINGULAR_INTERSECTIONS.put(Pair.of(ZERO, X), ZERO);
        SINGULAR_INTERSECTIONS.put(Pair.of(X, ZERO), ZERO);
        SINGULAR_INTERSECTIONS.put(Pair.of(X, X), X);
        SINGULAR_INTERSECTIONS.put(Pair.of(ONE, ONE), ONE);
        SINGULAR_INTERSECTIONS.put(Pair.of(ONE, X), ONE);
        SINGULAR_INTERSECTIONS.put(Pair.of(X, ONE), ONE);
        SINGULAR_INTERSECTIONS.put(Pair.of(ONE, ZERO), D);
        SINGULAR_INTERSECTIONS.put(Pair.of(ZERO, ONE), _D);
    }

    private final String name;

    Symbol(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public static Symbol intersectSingular(Symbol symbol1, Symbol symbol2) {
        return SINGULAR_INTERSECTIONS.get(Pair.of(symbol1, symbol2));
    }

    public static List<Symbol> intersectSingular(List<Symbol> row1, List<Symbol> row2) {
        List<Symbol> intersectionResult = new ArrayList<>();
        for (int i = 0; i < row1.size(); i++) {
            Symbol intersection = intersectSingular(row1.get(i), row2.get(i));
            intersectionResult.add(intersection);
        }
        return intersectionResult;
    }

    public static Symbol intersectDCoubElement(Symbol symbol1, Symbol symbol2) {
        if (symbol2 == symbol1 || symbol2 == X) {
            return symbol1;
        } else if (symbol1 == X) {
            return symbol2;
        } else {
            return null;
        }
    }
}

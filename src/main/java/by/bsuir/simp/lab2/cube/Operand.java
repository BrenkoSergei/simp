package by.bsuir.simp.lab2.cube;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum Operand {

    AND_2(Arrays.asList(
            SingularCube.of(Symbol.ZERO, Symbol.ZERO, Symbol.X),
            SingularCube.of(Symbol.ZERO, Symbol.X, Symbol.ZERO),
            SingularCube.of(Symbol.ONE, Symbol.ONE, Symbol.ONE)
    )),

    NOT_AND_2(Arrays.asList(
            SingularCube.of(Symbol.ZERO, Symbol.ONE, Symbol.ONE),
            SingularCube.of(Symbol.ONE, Symbol.ZERO, Symbol.X),
            SingularCube.of(Symbol.ONE, Symbol.X, Symbol.ZERO)
    )),

    AND_3(Arrays.asList(
            SingularCube.of(Symbol.ZERO, Symbol.ZERO, Symbol.X, Symbol.X),
            SingularCube.of(Symbol.ZERO, Symbol.X, Symbol.ZERO, Symbol.X),
            SingularCube.of(Symbol.ZERO, Symbol.X, Symbol.X, Symbol.ZERO),
            SingularCube.of(Symbol.ONE, Symbol.ONE, Symbol.ONE, Symbol.ONE)
    )),

    OR_2(Arrays.asList(
            SingularCube.of(Symbol.ZERO, Symbol.ZERO, Symbol.ZERO),
            SingularCube.of(Symbol.ONE, Symbol.ONE, Symbol.X),
            SingularCube.of(Symbol.ONE, Symbol.X, Symbol.ONE)
    )),

    NOT_OR_2(Arrays.asList(
            SingularCube.of(Symbol.ZERO, Symbol.ONE, Symbol.X),
            SingularCube.of(Symbol.ZERO, Symbol.X, Symbol.ONE),
            SingularCube.of(Symbol.ONE, Symbol.ZERO, Symbol.ZERO)
    )),

    NOT(Arrays.asList(
            SingularCube.of(Symbol.ZERO, Symbol.ONE),
            SingularCube.of(Symbol.ONE, Symbol.ZERO)
    ));

    private List<SingularCube> singularCubes;

    Operand(List<SingularCube> singularCubes) {
        this.singularCubes = singularCubes;
    }

    public List<SingularCube> getSingularCubes() {
        return singularCubes;
    }

    public static final class SingularCube {

        private final Symbol output;
        private final List<Symbol> inputs;

        private SingularCube(Symbol output, List<Symbol> inputs) {
            this.output = output;
            this.inputs = inputs;
        }

        public static SingularCube of(Symbol output, Symbol... inputs) {
            List<Symbol> inputSymbols = Stream.of(inputs).collect(Collectors.toList());
            return new SingularCube(output, inputSymbols);
        }

        public Symbol getOutput() {
            return output;
        }

        public List<Symbol> getInputs() {
            return inputs;
        }

        public List<Symbol> getFull() {
            return Stream.concat(inputs.stream(), Stream.of(output))
                    .collect(Collectors.toList());
        }

        public SingularCube withOutput(Symbol output) {
            return new SingularCube(output, inputs);
        }

        @Override
        public String toString() {
            String formattedInputs = inputs.stream().map(Objects::toString).collect(Collectors.joining(" - "));
            return formattedInputs + " - " + output;
        }
    }
}

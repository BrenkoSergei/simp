package by.bsuir.simp.lab2.test.calculator;

import by.bsuir.simp.lab2.cube.Symbol;

public interface Calculator {

    Symbol evaluate(Symbol symbol1, Symbol symbol2);
}

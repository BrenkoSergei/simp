package by.bsuir.simp.lab2.test;

import by.bsuir.simp.lab2.cube.Symbol;
import by.bsuir.simp.lab2.graph.Element;
import by.bsuir.simp.lab2.graph.InputNode;
import by.bsuir.simp.lab2.graph.Node;
import by.bsuir.simp.lab2.test.calculator.CalculatorRegistry;
import by.bsuir.simp.lab2.util.Pair;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class SolutionTester {

    public static Map<Pair<Element, Integer>, Map<String, Symbol>> test(Node rootNode,
                                                                        Iterable<? extends Element> nodesToTest,
                                                                        List<InputNode> inputNodes,
                                                                        Set<Map<String, Symbol>> testInputs) {
        Map<Pair<Element, Integer>, Map<String, Symbol>> detectedMalfunctions = new LinkedHashMap<>();

        nodesToTest.forEach(nodeToTest -> {
            for (int i = 0; i <= 1; i++) {
                for (Map<String, Symbol> testInput : testInputs) {
                    Map<Element, Symbol> outputValues = inputNodes.stream()
                            .collect(Collectors.toMap(Function.identity(), inputNode -> testInput.get(inputNode.toString())));
                    Symbol expectedOutput = evaluateSchemaWithValues(rootNode, outputValues);

                    outputValues.put(nodeToTest, i == 0 ? Symbol.ZERO : Symbol.ONE);
                    Symbol actualOutput = evaluateSchemaWithValues(rootNode, outputValues);

                    if (expectedOutput != actualOutput) {
                        detectedMalfunctions.put(Pair.of(nodeToTest, i), testInput);
                        break;
                    }
                }
            }
        });

        return detectedMalfunctions;
    }

    private static Symbol evaluateSchemaWithValues(Node node, Map<Element, Symbol> schemaElementToValue) {
        Symbol result = schemaElementToValue.get(node);
        if (result == null) {
            List<Symbol> inputs = node.getInputs().stream()
                    .map(element -> {
                        if (element instanceof InputNode) {
                            return schemaElementToValue.get(element);
                        } else {
                            return evaluateSchemaWithValues(((Node) element), schemaElementToValue);
                        }
                    })
                    .collect(Collectors.toList());
            if (inputs.size() == 1) {
                result = CalculatorRegistry.getCalculator(node.getOperand())
                        .evaluate(inputs.get(0), null);
            } else {
                result = inputs.get(0);
                for (int i = 1; i < inputs.size(); i++) {
                    result = CalculatorRegistry.getCalculator(node.getOperand())
                            .evaluate(result, inputs.get(i));
                }
            }
        }
        return result;
    }
}

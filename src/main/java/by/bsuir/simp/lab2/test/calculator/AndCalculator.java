package by.bsuir.simp.lab2.test.calculator;

import by.bsuir.simp.lab2.cube.Symbol;

public class AndCalculator implements Calculator {

    @Override
    public Symbol evaluate(Symbol symbol1, Symbol symbol2) {
        return symbol1 == Symbol.ONE && symbol2 == Symbol.ONE
                ? Symbol.ONE
                : Symbol.ZERO;
    }
}

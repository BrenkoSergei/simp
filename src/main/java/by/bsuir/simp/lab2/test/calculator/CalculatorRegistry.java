package by.bsuir.simp.lab2.test.calculator;

import java.util.EnumMap;
import java.util.Map;

import by.bsuir.simp.lab2.cube.Operand;

public class CalculatorRegistry {

    private static final Map<Operand, Calculator> OPERAND_TO_CALCULATOR;

    static {
        OPERAND_TO_CALCULATOR = new EnumMap<>(Operand.class);
        OPERAND_TO_CALCULATOR.put(Operand.AND_2, new AndCalculator());
        OPERAND_TO_CALCULATOR.put(Operand.AND_3, new AndCalculator());
        OPERAND_TO_CALCULATOR.put(Operand.NOT_AND_2, new NotAndCalculator());
        OPERAND_TO_CALCULATOR.put(Operand.OR_2, new OrCalculator());
        OPERAND_TO_CALCULATOR.put(Operand.NOT_OR_2, new NotOrCalculator());
        OPERAND_TO_CALCULATOR.put(Operand.NOT, new NotOrCalculator());
    }

    public static Calculator getCalculator(Operand operand) {
        return OPERAND_TO_CALCULATOR.get(operand);
    }
}

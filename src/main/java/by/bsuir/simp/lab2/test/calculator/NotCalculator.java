package by.bsuir.simp.lab2.test.calculator;

import by.bsuir.simp.lab2.cube.Symbol;

public class NotCalculator implements Calculator {

    @Override
    public Symbol evaluate(Symbol symbol1, Symbol symbol2) {
        return symbol1 == Symbol.ONE
                ? Symbol.ZERO
                : Symbol.ONE;
    }
}

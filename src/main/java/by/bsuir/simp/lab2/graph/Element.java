package by.bsuir.simp.lab2.graph;

import by.bsuir.simp.lab2.cube.Operand;

import java.util.List;

public interface Element {

    void setParent(Node element);

    Node getParent();

    List<Operand.SingularCube> getSingularCubes();
}

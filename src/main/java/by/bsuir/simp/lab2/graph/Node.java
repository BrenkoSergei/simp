package by.bsuir.simp.lab2.graph;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import by.bsuir.simp.lab2.cube.Operand;

public class Node implements Element {

    private final String name;
    private final Operand operand;
    private final List<Element> inputs;
    private Node parent;

    public Node(String name, Operand operand, Element firstInput, Element... inputs) {
        this.name = name;
        this.operand = operand;
        this.inputs = Stream.concat(Stream.of(firstInput), Stream.of(inputs))
                .peek(input -> input.setParent(this))
                .collect(Collectors.toList());
    }

    public Operand getOperand() {
        return operand;
    }

    public List<Element> getInputs() {
        return inputs;
    }

    @Override
    public void setParent(Node element) {
        this.parent = element;
    }

    @Override
    public Node getParent() {
        return parent;
    }

    @Override
    public List<Operand.SingularCube> getSingularCubes() {
        return operand.getSingularCubes();
    }

    @Override
    public String toString() {
        return name;
    }
}

package by.bsuir.simp.lab2.graph;

import by.bsuir.simp.lab2.cube.Operand;
import by.bsuir.simp.lab2.cube.Symbol;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InputNode implements Element {

    private static final List<Operand.SingularCube> INPUT_NODE_SINGULAR_CUBES = Arrays.asList(
            Operand.SingularCube.of(Symbol.ZERO, Symbol.ZERO),
            Operand.SingularCube.of(Symbol.ONE, Symbol.ONE)
    );

    private final int number;
    private Node parent;

    public InputNode(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "x" + number;
    }

    @Override
    public void setParent(Node element) {
        this.parent = element;
    }

    @Override
    public Node getParent() {
        return parent;
    }

    @Override
    public List<Operand.SingularCube> getSingularCubes() {
        return new ArrayList<>(INPUT_NODE_SINGULAR_CUBES);
    }
}

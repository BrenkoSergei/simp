package by.bsuir.simp.lab2;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import by.bsuir.simp.lab2.cube.Symbol;
import by.bsuir.simp.lab2.graph.Node;

public class FinalCalculator {

    private final List<String> allNodeNames;

    public FinalCalculator(List<String> allNodeNames) {
        this.allNodeNames = allNodeNames;
    }

    Map<String, Symbol> calculate(Map<String, Symbol> namedPrimitiveDCube,
                                  Map<Node, List<Map<String, Symbol>>> nodeOnWayToDCubes,
                                  Map<Node, List<Map<String, Symbol>>> nodeNotOnWayToSingularCubes) {
        Map<String, Symbol> intersections = intersectFullDCubes(generateFullDCube(), generateFullDCube(namedPrimitiveDCube));

        for (Map.Entry<Node, List<Map<String, Symbol>>> nodeOnWayToDCube : nodeOnWayToDCubes.entrySet()) {
            List<Map<String, Symbol>> dCubes = nodeOnWayToDCube.getValue();
            intersections = intersect(intersections, dCubes);
        }
        for (Map.Entry<Node, List<Map<String, Symbol>>> nodeNotOnWayToSingularCube : nodeNotOnWayToSingularCubes.entrySet()) {
            List<Map<String, Symbol>> dCubes = nodeNotOnWayToSingularCube.getValue();
            intersections = intersect(intersections, dCubes);
        }

        return intersections.keySet().stream()
                .filter(s -> s.startsWith("x"))
                .collect(Collectors.toMap(Function.identity(), intersections::get));
    }

    private Map<String, Symbol> intersect(Map<String, Symbol> accumulator, List<Map<String, Symbol>> namedDCubes) {
        return namedDCubes
                .stream()
                .map(namedDCube -> intersectFullDCubes(accumulator, generateFullDCube(namedDCube)))
                .filter(intersection -> !intersection.containsValue(null))
                .findFirst()
                .orElseThrow(RuntimeException::new);
    }

    private Map<String, Symbol> intersectFullDCubes(Map<String, Symbol> dCube1, Map<String, Symbol> dCube2) {
        Map<String, Symbol> result = generateFullDCube();

        dCube1.forEach((identifier, symbol1) -> {
            Symbol symbol2 = dCube2.get(identifier);
            Symbol intersection = Symbol.intersectDCoubElement(symbol1, symbol2);

            result.put(identifier, intersection);
        });
        return result;
    }

    private Map<String, Symbol> generateFullDCube() {
        return generateFullDCube(Collections.emptyMap());
    }

    private Map<String, Symbol> generateFullDCube(Map<String, Symbol> baseDCube) {
        Map<String, Symbol> namedDCube = allNodeNames.stream()
                .collect(Collectors.toMap(
                        Function.identity(),
                        identifier -> Symbol.X,
                        (symbol, symbol2) -> symbol,
                        LinkedHashMap::new
                ));
        namedDCube.putAll(baseDCube);
        return namedDCube;
    }
}

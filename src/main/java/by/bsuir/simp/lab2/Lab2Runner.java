package by.bsuir.simp.lab2;

import by.bsuir.simp.lab2.cube.Operand;
import by.bsuir.simp.lab2.cube.Symbol;
import by.bsuir.simp.lab2.graph.Element;
import by.bsuir.simp.lab2.graph.InputNode;
import by.bsuir.simp.lab2.graph.Node;
import by.bsuir.simp.lab2.test.SolutionTester;
import by.bsuir.simp.lab2.util.CollectionUtils;
import by.bsuir.simp.lab2.util.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Lab2Runner {

    public static void main(String[] args) {
        InputNode inputNode0 = new InputNode(0);
        InputNode inputNode1 = new InputNode(1);
        InputNode inputNode2 = new InputNode(2);
        InputNode inputNode3 = new InputNode(3);
        InputNode inputNode4 = new InputNode(4);
        InputNode inputNode5 = new InputNode(5);
        InputNode inputNode6 = new InputNode(6);
        List<InputNode> inputNodes = Arrays.asList(inputNode0, inputNode1, inputNode2, inputNode3, inputNode4, inputNode5, inputNode6);

        Node f1 = new Node("F1", Operand.NOT_AND_2, inputNode0, inputNode1);
        Node f2 = new Node("F2", Operand.NOT, inputNode2);
        Node f3 = new Node("F3", Operand.NOT_OR_2, inputNode4, inputNode5);
        Node f4 = new Node("F4", Operand.AND_3, inputNode3, f3, inputNode6);
        Node f5 = new Node("F5", Operand.OR_2, f2, f4);
        Node f6 = new Node("F6", Operand.AND_2, f1, f5);
        List<Node> nodes = Arrays.asList(f1, f2, f3, f4, f5, f6);

        List<? extends Element> allElements = Stream.concat(inputNodes.stream(), nodes.stream()).collect(Collectors.toList());

        Set<Map<String, Symbol>> testInputs = findTestInputs(inputNodes, nodes, allElements);
        System.out.println("\n================================================");
        System.out.println("========      Calculated sequences      ========");
        System.out.println("================================================");
        String header = testInputs.iterator().next().keySet().stream().map(s -> String.format("%4s", s)).collect(Collectors.joining());
        System.out.println(header);
        for (Map<String, Symbol> testInput : testInputs) {
            String content = testInput.keySet().stream().map(testInput::get).map(s -> String.format("%4s", s)).collect(Collectors.joining());
            System.out.println(content);
        }


        Map<Pair<Element, Integer>, Map<String, Symbol>> test = SolutionTester.test(f6, allElements, inputNodes, testInputs);
        System.out.println("\n\n\n\n============================================================");
        System.out.println("========         Check calculated sequences         ========");
        System.out.println("========      (only valid sequences displayed)      ========");
        System.out.println("============================================================");

        test.forEach((nodeMalformation, inputValues) -> {
            System.out.println("====     Element: " + nodeMalformation.getFirst() + " == " + nodeMalformation.getSecond() + "     ====");
            List<String> inputs = inputValues.keySet().stream()
                    .sorted()
                    .collect(Collectors.toList());
            String label = inputs.stream().map(s -> String.format("%4s", s)).collect(Collectors.joining());
            String content = inputs.stream().map(inputValues::get).map(s -> String.format("%4s", s)).collect(Collectors.joining());

            System.out.println(label);
            System.out.println(content);
        });
        System.out.println("============================================================");
    }

    public static Set<Map<String, Symbol>> findTestInputs(List<InputNode> inputNodes, List<Node> nodes, Iterable<? extends Element> targetElements) {
        List<String> allNodeNames = Stream.concat(
                inputNodes.stream().map(InputNode::toString),
                nodes.stream().map(Node::toString)
        ).collect(Collectors.toList());

        List<Map<String, Symbol>> inputsForTest = new ArrayList<>();
        for (Element target : targetElements) {
            Map<Node, List<Map<String, Symbol>>> nodeOnWayToNamedDCubes = new LinkedHashMap<>();
            for (Element node = target; node.getParent() != null; node = node.getParent()) {
                Node nodeOnWay = node.getParent();
                List<Map<String, Symbol>> dCubes = computeDCubes(nodeOnWay);
                nodeOnWayToNamedDCubes.put(nodeOnWay, dCubes);
            }

            Map<Node, List<Map<String, Symbol>>> nodeNotOnWayToSingularCubes = nodes.stream()
                    .filter(node -> !nodeOnWayToNamedDCubes.containsKey(node))
                    .filter(node -> !node.equals(target))
                    .sorted((node1, node2) -> node1.getParent().equals(node2) ? 1 : node2.getParent().equals(node1) ? -1 : 0)
                    .collect(Collectors.toMap(
                            Function.identity(),
                            Lab2Runner::mapToNamedSingularCube,
                            (node1, node2) -> node1,
                            LinkedHashMap::new
                    ));

            List<Operand.SingularCube> singularCubes = target.getSingularCubes();
            for (int i = 0; i <= 1; i++) {
                Operand.SingularCube cube = getSingularCubeWithOutput(singularCubes, i == 0 ? Symbol.ONE : Symbol.ZERO);
                cube = cube.withOutput(i == 0 ? Symbol.D : Symbol._D);
                Map<String, Symbol> namedPrimitiveDCube = mapToNames(target, cube);

                Map<String, Symbol> inputValues = new FinalCalculator(allNodeNames)
                        .calculate(namedPrimitiveDCube, nodeOnWayToNamedDCubes, nodeNotOnWayToSingularCubes);
                Symbol symbolOfTargetNode = inputValues.get(target.toString()); // when target node is input node
                if (symbolOfTargetNode != null) {
                    if (symbolOfTargetNode == Symbol.D) {
                        inputValues.put(target.toString(), Symbol.ONE);
                    } else if (symbolOfTargetNode == Symbol._D) {
                        inputValues.put(target.toString(), Symbol.ZERO);
                    }
                }
                inputsForTest.add(inputValues);
            }
        }

        return reducePossibleInputs(inputsForTest);
    }

    private static Set<Map<String, Symbol>> reducePossibleInputs(List<Map<String, Symbol>> inputsForTest) {
        Set<String> inputKeys = inputsForTest.get(0).keySet();

        inputKeys.forEach(inputKey -> {
            Set<Symbol> distinctValuesForInput = inputsForTest.stream()
                    .map(nameToSymbol -> nameToSymbol.get(inputKey))
                    .collect(Collectors.toSet());

            if (distinctValuesForInput.contains(Symbol.X)) {
                if (!distinctValuesForInput.contains(Symbol.ZERO) && distinctValuesForInput.contains(Symbol.ONE)) {
                    inputsForTest.forEach(stringSymbolMap ->
                            stringSymbolMap.computeIfPresent(inputKey, (s, symbol) -> symbol == Symbol.X ? Symbol.ONE : symbol)
                    );
                } else {
                    inputsForTest.forEach(stringSymbolMap ->
                            stringSymbolMap.computeIfPresent(inputKey, (s, symbol) -> symbol == Symbol.X ? Symbol.ZERO : symbol)
                    );
                }
            }
        });
        return new HashSet<>(inputsForTest);
    }

    private static List<Map<String, Symbol>> mapToNamedSingularCube(Node node) {
        return node.getOperand()
                .getSingularCubes()
                .stream()
                .map(singularCube -> mapToNames(node, singularCube))
                .collect(Collectors.toList());
    }

    private static Map<String, Symbol> mapToNames(Element node, Operand.SingularCube primitiveDCube) {
        Map<String, Symbol> identifierToSymbol = new HashMap<>();

        if (node instanceof Node) {
            identifierToSymbol.put(node.toString(), primitiveDCube.getOutput());
            for (int j = 0; j < primitiveDCube.getInputs().size(); j++) {
                identifierToSymbol.put(((Node) node).getInputs().get(j).toString(), primitiveDCube.getInputs().get(j));
            }
        } else {
//            identifierToSymbol.put(node.toString(), primitiveDCube.getInputs().get(0));
        }
        identifierToSymbol.put(node.toString(), primitiveDCube.getOutput());

        return identifierToSymbol;
    }

    private static List<Map<String, Symbol>> computeDCubes(Node node) {
        List<Operand.SingularCube> singularCubes = node.getOperand().getSingularCubes();
        Map<Symbol, List<List<Symbol>>> outputToSingularCube = singularCubes.stream()
                .collect(Collectors.groupingBy(
                        Operand.SingularCube::getOutput,
                        Collectors.mapping(Operand.SingularCube::getFull, Collectors.toList())
                ));

        List<List<Symbol>> cubesWithOutputOne = outputToSingularCube.get(Symbol.ONE);
        List<List<Symbol>> cubesWithOutputZero = outputToSingularCube.get(Symbol.ZERO);

        List<List<Symbol>> dCubes = CollectionUtils.computeCartesianProduct(cubesWithOutputZero, cubesWithOutputOne, Symbol::intersectSingular);
        dCubes.addAll(CollectionUtils.computeCartesianProduct(cubesWithOutputOne, cubesWithOutputZero, Symbol::intersectSingular));
        return dCubes.stream()
                .map(symbols -> mapDCubeToNames(node, symbols))
                .collect(Collectors.toList());
    }

    private static Map<String, Symbol> mapDCubeToNames(Node node, List<Symbol> dCube) {
        Map<String, Symbol> identifierToSymbol = new HashMap<>();
        for (int i = 0; i < dCube.size() - 1; i++) {
            identifierToSymbol.put(node.getInputs().get(i).toString(), dCube.get(i));
        }
        identifierToSymbol.put(node.toString(), dCube.get(dCube.size() - 1));
        return identifierToSymbol;
    }

    private static Operand.SingularCube getSingularCubeWithOutput(List<Operand.SingularCube> singularCubes, Symbol output) {
        return singularCubes.stream()
                .filter(singularCube -> output == singularCube.getOutput())
                .findAny()
                .orElse(null);
    }
}

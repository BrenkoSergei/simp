package by.bsuir.simp.lab1.strategy.impl;

import java.util.BitSet;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import by.bsuir.simp.lab1.graph.Element;
import by.bsuir.simp.lab1.graph.InputNode;
import by.bsuir.simp.lab1.graph.Node;
import by.bsuir.simp.lab1.strategy.AbstractBooleanMatchingStrategy;
import by.bsuir.simp.lab1.strategy.Strategy;
import by.bsuir.simp.lab1.strategy.StrategyResolver;
import by.bsuir.simp.lab1.util.BitSetUtil;

public class AnyOnesStrategy extends AbstractBooleanMatchingStrategy {

    public AnyOnesStrategy(StrategyResolver strategyResolver) {
        super(strategyResolver, true);
    }

    @Override
    public Set<BitSet> evaluate(List<Element> elements) {
        Map<Boolean, List<Element>> elementsByInstantiatingNode = elements.stream()
                .collect(Collectors.partitioningBy(element -> element instanceof Node));

        List<Element> nodes = elementsByInstantiatingNode.get(Boolean.TRUE);
        Set<BitSet> bitSets = nodes.stream()
                .map(element -> ((Node) element))
                .map(this::evaluateChildren)
                .reduce(this::reduceBitSets)
                .orElseGet(HashSet::new);

        List<Element> inputNodes = elementsByInstantiatingNode.get(Boolean.FALSE);
        Set<BitSet> elementBitSets = inputNodes.stream()
                .map(element -> ((InputNode) element))
                .map(this::toBitSet)
                .collect(Collectors.toSet());
        elementBitSets = addReducedBitSet(elementBitSets);

        return reduceBitSets(bitSets, elementBitSets);
    }

    private Set<BitSet> evaluateChildren(Node node) {
        Strategy<List<Element>> strategy = strategyResolver.resolve((node).getOperand(), true);
        return strategy.evaluate(node.getInputs());
    }

    private Set<BitSet> reduceBitSets(Set<BitSet> bitSets1, Set<BitSet> bitSets2) {
        Set<BitSet> cartesianProduct = BitSetUtil.doCartesianProduct(bitSets1, bitSets2);
        cartesianProduct.addAll(bitSets1);
        cartesianProduct.addAll(bitSets2);
        return cartesianProduct;
    }

    private BitSet toBitSet(InputNode inputNode) {
        BitSet bitSet = new BitSet();
        bitSet.set(inputNode.getNumber());
        return bitSet;
    }

    private Set<BitSet> addReducedBitSet(Set<BitSet> bitSets) {
        Set<BitSet> result = new HashSet<>(bitSets);
        bitSets.stream()
                .reduce(BitSetUtil::merge)
                .ifPresent(result::add);
        return result;
    }
}

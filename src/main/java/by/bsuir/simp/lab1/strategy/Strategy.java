package by.bsuir.simp.lab1.strategy;

import java.util.BitSet;
import java.util.Set;

public interface Strategy<T> {

    Set<BitSet> evaluate(T value);
}

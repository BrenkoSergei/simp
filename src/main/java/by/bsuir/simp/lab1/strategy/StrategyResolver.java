package by.bsuir.simp.lab1.strategy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.PostConstruct;

import by.bsuir.simp.lab1.graph.Element;
import by.bsuir.simp.lab1.operation.Operand;
import by.bsuir.simp.lab1.strategy.impl.AllOnesStrategy;
import by.bsuir.simp.lab1.strategy.impl.AllZerosStrategy;
import by.bsuir.simp.lab1.strategy.impl.AnyOnesStrategy;
import by.bsuir.simp.lab1.strategy.impl.AnyZerosStrategy;
import org.springframework.stereotype.Service;

@Service
public class StrategyResolver {

    private Map<Pair<Operand, Boolean>, Strategy<List<Element>>> operandAndDesiredValueToStrategy = new HashMap<>();

    @PostConstruct
    public void init() {
        operandAndDesiredValueToStrategy.put(Pair.of(Operand.AND, true), new AllOnesStrategy(this));
        operandAndDesiredValueToStrategy.put(Pair.of(Operand.AND, false), new AnyZerosStrategy(this));

        operandAndDesiredValueToStrategy.put(Pair.of(Operand.NOT_AND, true), new AnyZerosStrategy(this));
        operandAndDesiredValueToStrategy.put(Pair.of(Operand.NOT_AND, false), new AllOnesStrategy(this));

        operandAndDesiredValueToStrategy.put(Pair.of(Operand.OR, true), new AnyOnesStrategy(this));
        operandAndDesiredValueToStrategy.put(Pair.of(Operand.OR, false), new AllZerosStrategy(this));

        operandAndDesiredValueToStrategy.put(Pair.of(Operand.NOT_OR, true), new AllZerosStrategy(this));
        operandAndDesiredValueToStrategy.put(Pair.of(Operand.NOT_OR, false), new AnyOnesStrategy(this));

        operandAndDesiredValueToStrategy.put(Pair.of(Operand.NOT, true), new AllZerosStrategy(this));
        operandAndDesiredValueToStrategy.put(Pair.of(Operand.NOT, false), new AllOnesStrategy(this));
    }

    public Strategy<List<Element>> resolve(Operand operand, boolean desiredOutput) {
        return operandAndDesiredValueToStrategy.get(Pair.of(operand, desiredOutput));
    }

    public Strategy<List<Element>> resolveReverse(Operand operand, boolean desiredOutput) {
        return operandAndDesiredValueToStrategy.get(Pair.of(operand, !desiredOutput));
    }

    private static class Pair<F, S> {

        private F first;
        private S second;

        private Pair(F first, S second) {
            this.first = first;
            this.second = second;
        }

        private static <F, S> Pair<F, S> of(F first, S second) {
            return new Pair<>(first, second);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Pair<?, ?> pair = (Pair<?, ?>) o;
            return Objects.equals(first, pair.first)
                    && Objects.equals(second, pair.second);
        }

        @Override
        public int hashCode() {
            return Objects.hash(first, second);
        }
    }
}

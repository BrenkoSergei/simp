package by.bsuir.simp.lab1.strategy;

import java.util.List;

import by.bsuir.simp.lab1.graph.Element;

public abstract class AbstractBooleanMatchingStrategy implements Strategy<List<Element>> {

    protected final StrategyResolver strategyResolver;
    protected final boolean matchingValue;

    protected AbstractBooleanMatchingStrategy(StrategyResolver strategyResolver, boolean matchingValue) {
        this.strategyResolver = strategyResolver;
        this.matchingValue = matchingValue;
    }
}

package by.bsuir.simp.lab1.strategy.impl;

import java.util.BitSet;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import by.bsuir.simp.lab1.graph.Element;
import by.bsuir.simp.lab1.graph.Node;
import by.bsuir.simp.lab1.strategy.AbstractBooleanMatchingStrategy;
import by.bsuir.simp.lab1.strategy.Strategy;
import by.bsuir.simp.lab1.strategy.StrategyResolver;
import by.bsuir.simp.lab1.util.BitSetUtil;

public class AllZerosStrategy extends AbstractBooleanMatchingStrategy {

    public AllZerosStrategy(StrategyResolver strategyResolver) {
        super(strategyResolver, false);
    }

    @Override
    public Set<BitSet> evaluate(List<Element> elements) {
        Map<Boolean, List<Element>> elementsByInstantiatingNode = elements.stream()
                .collect(Collectors.partitioningBy(element -> element instanceof Node));

        List<Element> nodes = elementsByInstantiatingNode.get(Boolean.TRUE);
        return nodes.stream()
                .map(element -> ((Node) element))
                .map(this::evaluateChildren)
                .reduce(BitSetUtil::doCartesianProduct)
                .orElseGet(() -> Collections.singleton(new BitSet()));
    }

    private Set<BitSet> evaluateChildren(Node node) {
        Strategy<List<Element>> strategy = strategyResolver.resolve((node).getOperand(), false);
        return strategy.evaluate(node.getInputs());
    }
}

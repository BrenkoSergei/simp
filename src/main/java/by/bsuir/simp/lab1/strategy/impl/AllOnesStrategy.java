package by.bsuir.simp.lab1.strategy.impl;

import java.util.BitSet;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import by.bsuir.simp.lab1.graph.Element;
import by.bsuir.simp.lab1.graph.InputNode;
import by.bsuir.simp.lab1.graph.Node;
import by.bsuir.simp.lab1.strategy.AbstractBooleanMatchingStrategy;
import by.bsuir.simp.lab1.strategy.Strategy;
import by.bsuir.simp.lab1.strategy.StrategyResolver;
import by.bsuir.simp.lab1.util.BitSetUtil;

public class AllOnesStrategy extends AbstractBooleanMatchingStrategy {

    public AllOnesStrategy(StrategyResolver strategyResolver) {
        super(strategyResolver, true);
    }

    @Override
    public Set<BitSet> evaluate(List<Element> elements) {
        Map<Boolean, List<Element>> elementsByInstantiatingNode = elements.stream()
                .collect(Collectors.partitioningBy(element -> element instanceof Node));

        List<Element> nodes = elementsByInstantiatingNode.get(Boolean.TRUE);
        Set<BitSet> bitSets = nodes.stream()
                .map(element -> ((Node) element))
                .map(this::evaluateChildren)
                .reduce(BitSetUtil::doCartesianProduct)
                .orElseGet(() -> Collections.singleton(new BitSet()));

        List<Element> inputNodes = elementsByInstantiatingNode.get(Boolean.FALSE);
        inputNodes.stream()
                .map(element -> ((InputNode) element))
                .map(this::toBitSet)
                .reduce(BitSetUtil::merge)
                .ifPresent(reducedBitSet -> bitSets.forEach(bitSet -> bitSet.or(reducedBitSet)));
        return bitSets;
    }

    private Set<BitSet> evaluateChildren(Node node) {
        Strategy<List<Element>> strategy = strategyResolver.resolve((node).getOperand(), true);
        return strategy.evaluate(node.getInputs());
    }

    private BitSet toBitSet(InputNode inputNode) {
        BitSet bitSet = new BitSet();
        bitSet.set(inputNode.getNumber());
        return bitSet;
    }
}

package by.bsuir.simp.lab1.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InputNode implements Element {

    private final int number;
    private Element parent;

    public InputNode(int number) {
        this.number = number;
        Registry.register(this);
    }

    public int getNumber() {
        return number;
    }

    @Override
    public String toString() {
        return "x" + number;
    }

    @Override
    public void setParent(Element element) {
        this.parent = element;
    }

    @Override
    public Element getParent() {
        return parent;
    }

    public static class Registry {

        private static final Map<String, InputNode> NAME_TO_SCHEMA_INPUT = new HashMap<>();

        private static void register(InputNode inputNode) {
            NAME_TO_SCHEMA_INPUT.put(inputNode.toString(), inputNode);
        }

        public static List<String> getRegisteredNames() {
            return new ArrayList<>(NAME_TO_SCHEMA_INPUT.keySet());
        }
    }
}

package by.bsuir.simp.lab1.graph;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import by.bsuir.simp.lab1.operation.Operand;

public class Node implements Element {

    private final String name;
    private final Operand operand;
    private final List<Element> inputs;
    private Element parent;

    public Node(String name, Operand operand, Element firstInput, Element... inputs) {
        this.name = name;
        this.operand = operand;
        this.inputs = Stream.concat(Stream.of(firstInput), Stream.of(inputs))
                .collect(Collectors.toList());
    }

    public Operand getOperand() {
        return operand;
    }

    public List<Element> getInputs() {
        return inputs;
    }

    @Override
    public void setParent(Element element) {
        this.parent = element;
    }

    @Override
    public Element getParent() {
        return parent;
    }

    @Override
    public String toString() {
        return name;
    }
}

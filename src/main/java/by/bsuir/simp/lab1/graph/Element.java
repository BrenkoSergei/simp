package by.bsuir.simp.lab1.graph;

public interface Element {

    void setParent(Element element);

    Element getParent();
}

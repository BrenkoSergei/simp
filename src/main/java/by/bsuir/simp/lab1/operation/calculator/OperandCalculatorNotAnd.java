package by.bsuir.simp.lab1.operation.calculator;

import java.util.List;

import by.bsuir.simp.lab1.operation.Operand;
import org.springframework.stereotype.Service;

@Service
public class OperandCalculatorNotAnd extends OperandCalculatorAnd {

    @Override
    public Operand getOperand() {
        return Operand.NOT_AND;
    }

    @Override
    public boolean calculate(List<Boolean> inputs) {
        return !super.calculate(inputs);
    }
}

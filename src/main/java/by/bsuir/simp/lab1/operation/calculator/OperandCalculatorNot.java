package by.bsuir.simp.lab1.operation.calculator;

import java.util.List;

import by.bsuir.simp.lab1.operation.Operand;
import org.springframework.stereotype.Service;

@Service
public class OperandCalculatorNot implements OperandCalculator {

    @Override
    public Operand getOperand() {
        return Operand.NOT;
    }

    @Override
    public boolean calculate(List<Boolean> inputs) {
        if (inputs.size() == 1) {
            return !inputs.get(0);
        } else {
            throw new IllegalArgumentException();
        }
    }
}

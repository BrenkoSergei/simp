package by.bsuir.simp.lab1.operation.calculator;

import java.util.List;

import by.bsuir.simp.lab1.operation.Operand;
import org.springframework.stereotype.Service;

@Service
public class OperandCalculatorNotOr extends OperandCalculatorOr {

    @Override
    public Operand getOperand() {
        return Operand.NOT_OR;
    }

    @Override
    public boolean calculate(List<Boolean> inputs) {
        return !super.calculate(inputs);
    }
}

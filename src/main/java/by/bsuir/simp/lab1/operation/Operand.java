package by.bsuir.simp.lab1.operation;

public enum Operand {

    AND,
    NOT_AND,
    OR,
    NOT_OR,
    NOT
}

package by.bsuir.simp.lab1.operation.calculator;

import java.util.List;

import by.bsuir.simp.lab1.operation.Operand;

public interface OperandCalculator {

    Operand getOperand();

    boolean calculate(List<Boolean> inputs);
}

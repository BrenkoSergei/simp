package by.bsuir.simp.lab1.operation.calculator;

import java.util.List;

import by.bsuir.simp.lab1.operation.Operand;
import org.springframework.stereotype.Service;

@Service
public class OperandCalculatorOr implements OperandCalculator {

    @Override
    public Operand getOperand() {
        return Operand.OR;
    }

    @Override
    public boolean calculate(List<Boolean> inputs) {
        boolean result = false;
        for (Boolean input : inputs) {
            result |= input;
        }
        return result;
    }
}

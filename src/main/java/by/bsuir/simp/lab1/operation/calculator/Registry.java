package by.bsuir.simp.lab1.operation.calculator;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import by.bsuir.simp.lab1.operation.Operand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Registry {

    private final Map<Operand, OperandCalculator> operandToCalculator;

    @Autowired
    public Registry(List<OperandCalculator> calculators) {
        operandToCalculator = calculators.stream()
                .collect(Collectors.toMap(
                        OperandCalculator::getOperand,
                        Function.identity()
                ));
    }

    public OperandCalculator getCalculator(Operand operand) {
        return operandToCalculator.get(operand);
    }
}

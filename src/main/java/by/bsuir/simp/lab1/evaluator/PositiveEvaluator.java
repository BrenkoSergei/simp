package by.bsuir.simp.lab1.evaluator;

import java.util.BitSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import by.bsuir.simp.lab1.graph.Element;
import by.bsuir.simp.lab1.graph.InputNode;
import by.bsuir.simp.lab1.graph.Node;
import by.bsuir.simp.lab1.operation.calculator.OperandCalculator;
import by.bsuir.simp.lab1.operation.calculator.Registry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PositiveEvaluator {

    private final Registry calculatorRegistry;

    @Autowired
    public PositiveEvaluator(Registry calculatorRegistry) {
        this.calculatorRegistry = calculatorRegistry;
    }

    public Map<BitSet, Boolean> evaluate(Node root) {
        int numberOfInputs = InputNode.Registry.getRegisteredNames().size();

        return Stream.iterate(0, i -> ++i)
                .limit((long) Math.pow(2, numberOfInputs))
                .map(integer -> toBitSet(numberOfInputs, integer))
                .collect(Collectors.toMap(
                        Function.identity(),
                        bitSet -> doCalculation(bitSet, root),
                        (b1, b2) -> b1,
                        LinkedHashMap::new
                ));
    }

    private boolean doCalculation(BitSet bitSet, Node node) {
        List<Element> nodeInputs = node.getInputs();
        List<Boolean> inputs = nodeInputs.stream()
                .map(input -> {
                    if (input instanceof Node) {
                        return doCalculation(bitSet, (Node) input);
                    } else {
                        return bitSet.get(((InputNode) input).getNumber());
                    }
                })
                .collect(Collectors.toList());

        OperandCalculator calculator = calculatorRegistry.getCalculator(node.getOperand());
        return calculator.calculate(inputs);
    }

    private BitSet toBitSet(int numberOfBits, int integer) {
        BitSet bitSet = new BitSet(numberOfBits);
        for (int i = 0; i < numberOfBits; i++) {
            if (integer % 2 == 1) {
                bitSet.set(numberOfBits - 1 - i);
            }
            integer /= 2;
        }
        return bitSet;
    }
}

package by.bsuir.simp.lab1.util;

import java.util.BitSet;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public final class BitSetUtil {

    private BitSetUtil() {
    }

    public static Set<BitSet> doCartesianProduct(Set<BitSet> bitSets1, Set<BitSet> bitSets2) {
        if (bitSets1.isEmpty()) {
            return new HashSet<>(bitSets2);
        } else {
            return bitSets1.stream()
                    .flatMap(bitSet1 -> bitSets2.stream()
                            .map(bitSet2 -> merge(bitSet1, bitSet2)))
                    .collect(Collectors.toSet());
        }
    }

    public static BitSet merge(BitSet bitSet1, BitSet bitSet2) {
        BitSet set = new BitSet();
        set.or(bitSet1);
        set.or(bitSet2);
        return set;
    }
}

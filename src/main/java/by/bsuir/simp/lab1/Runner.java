package by.bsuir.simp.lab1;

import java.util.BitSet;
import java.util.List;
import java.util.Set;

import by.bsuir.simp.lab1.evaluator.PositiveEvaluator;
import by.bsuir.simp.lab1.graph.Element;
import by.bsuir.simp.lab1.graph.InputNode;
import by.bsuir.simp.lab1.graph.Node;
import by.bsuir.simp.lab1.operation.Operand;
import by.bsuir.simp.lab1.strategy.Strategy;
import by.bsuir.simp.lab1.strategy.StrategyResolver;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Runner {

    public static void main(String[] args) {
        InputNode inputNode0 = new InputNode(0);
        InputNode inputNode1 = new InputNode(1);
        InputNode inputNode2 = new InputNode(2);
        InputNode inputNode3 = new InputNode(3);
        InputNode inputNode4 = new InputNode(4);
        InputNode inputNode5 = new InputNode(5);
        InputNode inputNode6 = new InputNode(6);

        Node f1 = new Node("F1", Operand.NOT_AND, inputNode0, inputNode1);
        Node f2 = new Node("F2", Operand.NOT, inputNode2);
        Node f3 = new Node("F3", Operand.NOT_OR, inputNode4, inputNode5);
        Node f4 = new Node("F4", Operand.AND, inputNode3, f3, inputNode6);
        Node f5 = new Node("F5", Operand.OR, f2, f4);
        Node f6 = new Node("F6", Operand.AND, f1, f5);

        ApplicationContext applicationContext = new AnnotationConfigApplicationContext("by.bsuir.simp.lab1");
        PositiveEvaluator positiveEvaluator = applicationContext.getBean(PositiveEvaluator.class);
        positiveEvaluator.evaluate(f6);

        StrategyResolver strategyResolver = applicationContext.getBean(StrategyResolver.class);

        Node target = f6;
        Strategy<List<Element>> strategy = strategyResolver.resolve(target.getOperand(), false);
        Set<BitSet> evaluate = strategy.evaluate(target.getInputs());
        
        

        evaluate.forEach(System.out::println);
    }
}